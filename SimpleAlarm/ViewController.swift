//
//  ViewController.swift
//  SimpleAlarm
//
//  Created by Chris Wong on 18/11/2015.
//  Copyright © 2015 Chris Wong. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var application:UIApplication {
        get {
            return UIApplication.sharedApplication()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func cancelAllAlarmDidTap(sender: AnyObject) {
        let notificationCount = application.scheduledLocalNotifications?.count
        let alert = UIAlertController(title: "", message: "Cancal all alarm? Currently : \(notificationCount!)", preferredStyle: .Alert)
        let alertCancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) -> Void in
            
        }
        alert.addAction(alertCancelAction)
        
        let alertConfirmAction = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.Destructive) { (action) -> Void in
            if( self.application.scheduledLocalNotifications?.count > 0) {
                self.application.cancelAllLocalNotifications()
                self.application.applicationIconBadgeNumber = 0
            }
        }
        alert.addAction(alertConfirmAction)
        
        presentViewController(alert, animated: true) { () -> Void in
            
        }
    }
    
    @IBAction func addButtonDidTap(sender: UIButton) {
        let notification = UILocalNotification()
        notification.fireDate = NSDate().dateByAddingTimeInterval(60*60)
        notification.timeZone = NSTimeZone.defaultTimeZone()
        notification.alertBody = "Time up!!!"
        notification.alertTitle = "Hello!"
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.applicationIconBadgeNumber = 1
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
        
        let alert = UIAlertController(title: "", message: "Alarm is added", preferredStyle: .Alert)
        let alertAction = UIAlertAction(title: "OK", style: .Cancel) { (action) -> Void in
            
        }
        alert.addAction(alertAction)
        presentViewController(alert, animated: true) { () -> Void in
            
        }
    }

}

